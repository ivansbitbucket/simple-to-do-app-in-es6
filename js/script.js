'use esversion: 6'; // Enabling strict mode
// Declaring variables
let i;
let document; // Create document variable
let myNodelist = document.getElementsByTagName("LI"); // Get whole LIS on the page
let close = document.getElementsByClassName("close"); // Get previously created close element
// Create a "close" button and append it to each list item
for ( i = 0; i < myNodelist.length; i++) {
  let span = document.createElement("span"); //Create html element span which co
  let txt = document.createTextNode("\u00D7"); //Create cross icon
  span.className = "close alert button"; //Add class to created element span
  span.appendChild(txt); //Add cross icon to span element
  myNodelist[i].appendChild(span);//Adding close icon to each created element
}
// Function for add style for hiding elements
for (i = 0; i < close.length; i++) {
  close[i].onclick = () => {
    let div = this.parentElement;
    div.style.display = "none";
  }
}
// Create a new item when clicked on the add btn
   var addElementToList = () => {
      let li = document.createElement("li"); // Create New element
      let inputFieldText = document.getElementById("inputField"); // Get the input
      let inputValue = document.getElementById("inputField").value; //Get the input field value
      let t = document.createTextNode(inputValue); // Create node of inputval
      let span = document.createElement("span"); // Create span eleent
      let txt = document.createTextNode("\u00D7"); // Create croos icon
      li.appendChild(t); // Append previous inputval to created list item
      // Validate if input is empty add class error or if not add element
      if (inputValue === '') {
        inputFieldText.classList.add('error'); // If input is empty add error class
      } else {
        document.getElementById("myList").appendChild(li); // If input isn't empty add element
        inputFieldText.classList.remove('error'); // If not remove class
      }
      document.getElementById("myList").value = ""; // get the val of created samplelist
      span.className = "close close alert button"; // add the close class to created prev list
      span.appendChild(txt); // add the cross icon 
      li.appendChild(span); // add the close element to created li element
      // Loop iterating on elements and add style to close li element
      for (i = 0; i < close.length; i++) {
        close[i].onclick = () => {
          let div = this.parentElement;
          div.style.display = "none";
        }
      }
  }